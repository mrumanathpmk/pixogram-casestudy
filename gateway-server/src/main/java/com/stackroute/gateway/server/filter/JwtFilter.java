package com.stackroute.gateway.server.filter;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.stackroute.gateway.server.config.JwtFilterConfig;
import com.stackroute.gateway.server.util.Constants;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

/* This class implements the custom filter by extending org.springframework.web.filter.GenericFilterBean.  
 * Override the doFilter method with ServletRequest, ServletResponse and FilterChain.
 * This is used to authorize the API access for the application.
 */

@Component
public class JwtFilter extends GenericFilterBean {
	private static final String BEARER_STRING = "Bearer";
	@Autowired
	private JwtFilterConfig config;
	private static final Logger logger = LoggerFactory.getLogger(JwtFilter.class);
	/*
	 * Override the doFilter method of GenericFilterBean. Retrieve the
	 * "authorization" header from the HttpServletRequest object. Retrieve the
	 * "Bearer" token from "authorization" header. If authorization header is
	 * invalid, throw Exception with message. Parse the JWT token and get claims
	 * from the token using the secret key Set the request attribute with the
	 * retrieved claims Call FilterChain object's doFilter() method
	 */

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest req = (HttpServletRequest) request;
		final HttpServletResponse res = (HttpServletResponse) response;
		try {
			final String authHeader = req.getHeader("authorization");
			final String requestUrl = req.getRequestURI();
			AtomicBoolean ignoreUrl = new AtomicBoolean(false);

			config.getUrlExclusionList().forEach((url) -> {
				if (requestUrl.contains(url.toLowerCase())) {
					ignoreUrl.set(true);
				}
			});

			if (!ignoreUrl.get()) {
				if (!StringUtils.hasText(authHeader) || !authHeader.startsWith(BEARER_STRING)) {
					throw new ServletException("Missing or Invalid Authorization header");
				} else {
					final Claims claims = Jwts.parser().setSigningKey("secretKey")
							.parseClaimsJws(authHeader.substring(BEARER_STRING.length()).trim()).getBody();
					req.setAttribute(Constants.CLAIMS, claims);
				}
			}
			chain.doFilter(req, res);
		} catch (ExpiredJwtException eje) {
			logger.info("Security exception for user {} - {}", eje.getClaims().getSubject(), eje.getMessage());

			logger.trace("Security exception trace: {}", eje);
			res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		}

	}

}
