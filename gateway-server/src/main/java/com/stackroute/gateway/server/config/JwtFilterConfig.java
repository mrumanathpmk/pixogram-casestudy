package com.stackroute.gateway.server.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("jwt-filter")
public class JwtFilterConfig {

	private List<String> urlExclusionList;

	public List<String> getUrlExclusionList() {
		return urlExclusionList;
	}

	public void setUrlExclusionList(List<String> urlExclusionList) {
		this.urlExclusionList = urlExclusionList;
	}

}