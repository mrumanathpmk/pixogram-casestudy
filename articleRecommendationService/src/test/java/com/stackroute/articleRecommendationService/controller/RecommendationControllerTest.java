package com.stackroute.articleRecommendationService.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.articleRecommendationService.model.Article;
import com.stackroute.articleRecommendationService.service.RecommendationService;
import com.stackroute.articleRecommendationService.util.Constants;

import io.jsonwebtoken.Claims;

@RunWith(SpringRunner.class)
@WebMvcTest
public class RecommendationControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private RecommendationService service;

	private List<Article> articles;

	@InjectMocks
	private RecommendationController controller;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
		articles = new ArrayList<Article>();
		final Article article = new Article();
		article.setAuthor("Prabakaran");
		article.setTitle("Test case");
		articles.add(article);
	}

	@Test
	public void testGetAllRecommendedArticlesFailure() throws Exception {
		mvc.perform(get("/api/v1/recommendation").contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testGetAllRecommendedArticlesSuccess() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.getRecommendation("prabakaran")).thenReturn(articles);
		mvc.perform(get("/api/v1/recommendation").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
				.andExpect(content().string(jsonToString(articles)));
	}

	/**
	 * Parsing String format data into JSON format
	 * 
	 * @param obj
	 * @return
	 * @throws JsonProcessingException
	 */
	private static String jsonToString(final Object obj) throws JsonProcessingException {
		String result;
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(obj);
			result = jsonContent;
		} catch (JsonProcessingException e) {
			result = "Json processing error";
		}
		return result;
	}

}
