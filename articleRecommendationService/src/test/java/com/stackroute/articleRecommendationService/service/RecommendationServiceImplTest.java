package com.stackroute.articleRecommendationService.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stackroute.articleRecommendationService.model.Article;
import com.stackroute.articleRecommendationService.model.Favourite;
import com.stackroute.articleRecommendationService.repository.RecommendationRepository;
import com.stackroute.articleRecommendationService.service.impl.RecommendationServiceImpl;

public class RecommendationServiceImplTest {

	@Mock
	private RecommendationRepository repository;
	
	private Article article;
	
	@InjectMocks
	private RecommendationServiceImpl service;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		article = new Article();
		article.setId(new ObjectId(new Date()));
		article.setAuthor("praba");
		article.setTitle("junit");
		article.setDescription("junit for test cases");
	}
	
	@Test
	public void addRecommendationSuccess() {
		Favourite fav = new Favourite(article.getId(), article);
		fav.getUsers().add("prabakaran");
		when(repository.findById(article.getId())).thenReturn(Optional.empty());
		assertEquals(fav.toString(), service.addRecommendation("prabakaran", article).toString());
	}
	
	@Test
	public void addRecommendationFailure() {
		when(repository.findById(article.getId())).thenThrow(NoSuchElementException.class);
		assertNull(service.addRecommendation("prabakaran", article));
	}
	
	@Test
	public void removeRecommendationSuccess() {
		Favourite fav = new Favourite(article.getId(), article);
		fav.getUsers().add("prabakaran");
		when(repository.findById(article.getId())).thenReturn(Optional.of(fav));
		assertTrue(service.removeRecommendation("prabakaran", article));
	}
	
	@Test
	public void removeRecommendationFailure() {
		when(repository.findById(article.getId())).thenThrow(NoSuchElementException.class);
		assertFalse(service.removeRecommendation("prabakaran", article));
	}
	
	@Test
	public void getRecommendations() {
		List<Favourite> favourites = new ArrayList<Favourite>();
		Favourite fav1 = new Favourite(article.getId(), article);
		fav1.getUsers().add("prabakaran");
		favourites.add(fav1);
		Favourite fav2 = new Favourite(new ObjectId(new Date()), article);
		fav2.getUsers().add("junit");
		favourites.add(fav2);
		Favourite fav3 = new Favourite(new ObjectId(new Date()), article);
		fav3.getUsers().add("junit");
		fav3.getUsers().add("prabakaran");
		favourites.add(fav3);
		when(repository.findAll()).thenReturn(favourites);
		List<Article> result = service.getRecommendation("prabakaran");
		assertEquals(1, result.size());
		assertEquals(fav2.getArticle(), result.get(0));
	}
}
