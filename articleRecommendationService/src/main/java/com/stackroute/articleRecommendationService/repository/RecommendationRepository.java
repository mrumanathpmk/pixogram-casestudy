package com.stackroute.articleRecommendationService.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.stackroute.articleRecommendationService.model.Favourite;

@Repository
public interface RecommendationRepository extends MongoRepository<Favourite, ObjectId> {
	
}
