package com.stackroute.articleRecommendationService.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.articleRecommendationService.model.Article;
import com.stackroute.articleRecommendationService.service.RecommendationService;
import com.stackroute.articleRecommendationService.util.Constants;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.ApiImplicitParam;

@RestController
@RequestMapping("api/v1/recommendation")
public class RecommendationController {

	@Autowired
	private RecommendationService service;

	@GetMapping
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<?> getAllRecommendedArticles(final
	HttpServletRequest req) throws Exception {
		final Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
		if (null == claims || null == claims.getSubject()) {
			return new ResponseEntity<>("BAD request- Invalid headers received", HttpStatus.BAD_REQUEST);
		}
		final String username = claims.getSubject();
		return new ResponseEntity<List<Article>>(service.getRecommendation(username), HttpStatus.OK);
	}
}
