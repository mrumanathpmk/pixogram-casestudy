package com.stackroute.articleRecommendationService.service;

import java.util.List;

import com.stackroute.articleRecommendationService.model.Article;
import com.stackroute.articleRecommendationService.model.Favourite;

public interface RecommendationService {

	Favourite addRecommendation(String username, Article article);

	boolean removeRecommendation(String username, Article article);

	List<Article> getRecommendation(String username);

}
