package com.stackroute.articleRecommendationService.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.articleRecommendationService.model.Article;
import com.stackroute.articleRecommendationService.model.Favourite;
import com.stackroute.articleRecommendationService.repository.RecommendationRepository;
import com.stackroute.articleRecommendationService.service.RecommendationService;

@Service
public class RecommendationServiceImpl implements RecommendationService {

	private static final Logger logger = LoggerFactory.getLogger(RecommendationServiceImpl.class);
	@Autowired
	private RecommendationRepository repository;

	@Override
	public Favourite addRecommendation(String username, Article article) {
		try {
			Optional<Favourite> fav = repository.findById(article.getId());
			Favourite toSave;
			if (fav.isPresent()) {
				toSave = fav.get();
			} else {
				toSave = new Favourite(article.getId(), article);
			}
			toSave.getUsers().add(username);
			repository.save(toSave);
			return toSave;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public List<Article> getRecommendation(String userId) {
		List<Favourite> favourites = repository.findAll();
		List<Article> recommended = new ArrayList<Article>();
		for (Favourite favourite : favourites) {
			if (!favourite.getUsers().isEmpty() && !favourite.getUsers().contains(userId)) {
				recommended.add(favourite.getArticle());
			}
		}
		return recommended;
	}

	@Override
	public boolean removeRecommendation(String userId, Article article) {
		try {
			Optional<Favourite> fav = repository.findById(article.getId());
			if (fav.isPresent()) {
				final Favourite toSave = fav.get();
				toSave.getUsers().remove(userId);
				repository.save(toSave);
				return true;
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return false;
	}

}
