package com.stackroute.articleRecommendationService.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

import com.stackroute.articleRecommendationService.model.FavouriteMessage;
import com.stackroute.articleRecommendationService.service.RecommendationService;

@EnableBinding(Sink.class)
@Component
public class FavouriteConsumer {

	private static final Logger logger = LoggerFactory.getLogger(FavouriteConsumer.class);

	@Autowired
	private RecommendationService service;

	@StreamListener(target = Sink.INPUT)
	public void processFavourites(FavouriteMessage message) {
		if (null != message && null != message.getArticle() && null != message.getUsername()) {
			switch (message.getAction()) {
			case ADD:
				service.addRecommendation(message.getUsername(), message.getArticle());
				break;
			case REMOVE:
				service.removeRecommendation(message.getUsername(), message.getArticle());
				break;
			default:
				logger.warn("Invalid Action-" + message.getAction());
				break;
			}
		} else {
			logger.warn("invalid message format");
		}
	}

}
