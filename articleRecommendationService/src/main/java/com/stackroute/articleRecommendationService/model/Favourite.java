package com.stackroute.articleRecommendationService.model;

import java.util.HashSet;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document
public class Favourite {

	@Id
	@JsonProperty("id")
	private ObjectId id;

	private Article article;

	@JsonIgnore
	private Set<String> users;

	public Favourite() {
		super();
	}

	public Favourite(ObjectId id, Article article) {
		this.id = id;
		this.article = article;
	}

	@JsonIgnore
	public Set<String> getUsers() {
		if(null == users) {
			users = new HashSet<String>();
		}
		return users;
	}

	@JsonIgnore
	public void setUsers(Set<String> users) {
		this.users = users;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	@Override
	public String toString() {
		return "Favourite [id=" + id + ", article=" + article + ", users=" + users + "]";
	}
	
	

}