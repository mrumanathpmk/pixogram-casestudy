package com.stackroute.search.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.search.model.Article;
import com.stackroute.search.model.NewsObject;
import com.stackroute.search.model.Source;
import com.stackroute.search.service.SearchNewsService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class SearchNewsControllerTest {

	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private SearchNewsService service;
	
	@MockBean
	private SimpleDateFormat formatter;
	
	private SimpleDateFormat localFormatter = new SimpleDateFormat("dd/MM/yyyy");
	private NewsObject newsObj;
	
	@InjectMocks
	private SearchNewsController controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
		List<Article> articles = new ArrayList<Article>();
		Article article = new Article();
		article.setAuthor("Prabakaran");
		article.setTitle("Test case");
		article.setSource(new Source());
		article.getSource().setId("junit");
		article.getSource().setName("junit");
		articles.add(article);
		newsObj = new NewsObject();
		newsObj.setArticles(articles);
	}

	@Test
	public void testGetSearchResultSuccess() throws JsonProcessingException, Exception {
		when(service.search("apple")).thenReturn(newsObj);
		mvc.perform(get("/api/v1/search?q=apple")).andExpect(status().isOk()).andExpect(MockMvcResultMatchers.content().string(jsonToString(newsObj.getArticles())));
	}
	
	@Test
	public void testGetSearchResultWithStartDateOnlyBadRequest() throws JsonProcessingException, Exception {
		when(service.search("apple")).thenReturn(newsObj);
		mvc.perform(get("/api/v1/search?q=apple&from=09/09/2019")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void testGetSearchResultWithStartAndEndDatesuccess() throws JsonProcessingException, Exception {
		when(formatter.parse("09/09/2019")).thenReturn(localFormatter.parse("09/09/2019"));
		when(formatter.parse("10/10/2019")).thenReturn(localFormatter.parse("10/10/2019"));
		when(service.search("apple", "09/09/2019", "10/10/2019")).thenReturn(newsObj);
		mvc.perform(get("/api/v1/search?q=apple&from=09/09/2019&to=10/10/2019")).andExpect(status().isOk()).andExpect(MockMvcResultMatchers.content().string(jsonToString(newsObj.getArticles())));
	}
	
	@Test
	public void testGetSearchResultWithEndDateOnlyBadRequest() throws JsonProcessingException, Exception {
		when(service.search("apple")).thenReturn(newsObj);
		mvc.perform(get("/api/v1/search?q=apple&to=09/09/2019")).andExpect(status().isBadRequest());
	}
	
	@Test
	public void testGetSearchResultInternalServerError() throws JsonProcessingException, Exception {
		mvc.perform(get("/api/v1/search?q=apple")).andExpect(status().isInternalServerError());
	}
	
	/** Parsing String format data into JSON format
	 * 
	 * @param obj
	 * @return
	 * @throws JsonProcessingException
	 */
    private static String jsonToString(final Object obj) throws JsonProcessingException {
        String result;
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            result = jsonContent;
        } catch (JsonProcessingException e) {
            result = "Json processing error";
        }
        return result;
    }

}
