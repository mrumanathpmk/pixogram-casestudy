package com.stackroute.search.service;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.stackroute.search.exception.NewsServiceClientException;
import com.stackroute.search.external.client.NewsApiClient;
import com.stackroute.search.model.NewsObject;
import com.stackroute.search.service.impl.SearchNewsServiceImpl;

public class SearchNewsServiceImplTest {

	@Mock
	private NewsApiClient client;
	
	@InjectMocks
	private SearchNewsServiceImpl service;
	
	private NewsObject newsObj;
	private final String apiKey = "tempkey";
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(service, "clientNewsApiToken", apiKey, String.class);
		
		newsObj = new NewsObject();
		newsObj.setCode("200");
		newsObj.setTotalResults(1);
		newsObj.setMessage("Successful");
		newsObj.setStatus("success");
	}
	
	@Test
	public void testSearchWithOnlyQString() throws NewsServiceClientException {
		final String qString = "ibm";
		when(client.search(apiKey, qString)).thenReturn(newsObj);
		Assert.assertEquals(newsObj, service.search(qString));
	}
	
	@Test(expected= NewsServiceClientException.class)
	public void testSearchWithOnlyQStringError() throws NewsServiceClientException {
		final String qString = "ibm";
		newsObj.setStatus("error");
		when(client.search(apiKey, qString)).thenReturn(newsObj);
		Assert.assertEquals(newsObj, service.search(qString));
	}
	
	@Test(expected= NewsServiceClientException.class)
	public void testSearchWithOnlyQStringExternalError() throws NewsServiceClientException {
		final String qString = "ibm";
		when(client.search(apiKey, qString)).thenThrow(NullPointerException.class);
		Assert.assertEquals(newsObj, service.search(qString));
	}

	@Test
	public void testSearchWithQStringBeginAndEndDate() throws NewsServiceClientException {
		final String qString = "ibm";
		final String beginDate = "09/09/2019";
		final String endDate = "10/10/2019";
		when(client.search(apiKey, qString, beginDate, endDate)).thenReturn(newsObj);
		Assert.assertEquals(newsObj, service.search(qString, beginDate, endDate));
	}
	
	@Test(expected= NewsServiceClientException.class)
	public void testSearchWithQStringBeginAndEndDateError() throws NewsServiceClientException {
		final String qString = "ibm";
		newsObj.setStatus("error");
		final String beginDate = "09/09/2019";
		final String endDate = "10/10/2019";
		when(client.search(apiKey, qString, beginDate, endDate)).thenReturn(newsObj);
		Assert.assertEquals(newsObj, service.search(qString, beginDate, endDate));
	}
	
	@Test(expected= NewsServiceClientException.class)
	public void testSearchWithQStringBeginAndEndDateExternalError() throws NewsServiceClientException {
		final String qString = "ibm";
		final String beginDate = "09/09/2019";
		final String endDate = "10/10/2019";
		when(client.search(apiKey, qString, beginDate, endDate)).thenThrow(NullPointerException.class);
		Assert.assertEquals(newsObj, service.search(qString, beginDate, endDate));
		Assert.assertEquals(newsObj, service.search(qString));
	}

}
