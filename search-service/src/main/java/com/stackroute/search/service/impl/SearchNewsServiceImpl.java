package com.stackroute.search.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stackroute.search.exception.NewsServiceClientException;
import com.stackroute.search.external.client.NewsApiClient;
import com.stackroute.search.model.NewsObject;
import com.stackroute.search.service.SearchNewsService;

@Service
public class SearchNewsServiceImpl implements SearchNewsService {

	@Autowired
	private NewsApiClient client;

	@Value("${client.news-api.token}")
	private String clientNewsApiToken;

	@Override
	public NewsObject search(String qString) throws NewsServiceClientException {
		try {
			NewsObject obj = client.search(clientNewsApiToken, qString);
			if ("error".equalsIgnoreCase(obj.getStatus())) {
				throw new NewsServiceClientException(obj.getMessage());
			}
			return obj;
		} catch (Exception e) {
			throw new NewsServiceClientException(e.getMessage());
		}
	}

	@Override
	public NewsObject search(String qString, String beginTimeStamp, String endTimeStamp) throws NewsServiceClientException {
		try {
			NewsObject obj = client.search(clientNewsApiToken, qString, beginTimeStamp, endTimeStamp);
			if ("error".equalsIgnoreCase(obj.getStatus())) {
				throw new NewsServiceClientException(obj.getMessage());
			}
			return obj;
		} catch (Exception e) {
			throw new NewsServiceClientException(e.getMessage());
		}
	}

}
