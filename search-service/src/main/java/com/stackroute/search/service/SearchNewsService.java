package com.stackroute.search.service;

import com.stackroute.search.exception.NewsServiceClientException;
import com.stackroute.search.model.NewsObject;

public interface SearchNewsService {
	NewsObject search(String qString) throws NewsServiceClientException;
	NewsObject search(String qString, String beginTimeStamp, String endTimeStamp) throws NewsServiceClientException;
}
