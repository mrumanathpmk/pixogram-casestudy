package com.stackroute.search.config;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * general configuration class
 * 
 * @author Umanath Rajan
 *
 */
@Configuration
public class Config {

	@Bean
	public SimpleDateFormat dateFormatterForNewsApiClient(
			@Value("${client.news-api.api.general.date.format:yyyy-MM-dd}") String dateFormat) {
		return new SimpleDateFormat(dateFormat);
	}

}
