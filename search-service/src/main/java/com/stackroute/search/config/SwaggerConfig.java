package com.stackroute.search.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger implementation
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	/** group name from property file */
	@Value("${swagger.group}")
	private String groupName;

	/** title from property file */
	@Value("${swagger.title}")
	private String title;

	/** description from property file */
	@Value("${swagger.description}")
	private String description;

	/** url for terms and conditions */
	@Value("${swagger.url-for-terms}")
	private String urlForTerms;

	/** name of the contact person */
	@Value("${swagger.contact.name}")
	private String contactName;

	/** url of contact person */
	@Value("${swagger.contact.url}")
	private String contactUrl;

	/** mail id of contact person */
	@Value("${swagger.contact.mail}")
	private String contactMail;

	/** license name */
	@Value("${swagger.license}")
	private String license;

	/** license url */
	@Value("${swagger.license-url}")
	private String licenseUrl;

	/** default value */
	private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<String>(
			Arrays.asList(MediaType.APPLICATION_JSON_VALUE));

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName(groupName).apiInfo(getApiInfo())
				.consumes(DEFAULT_PRODUCES_AND_CONSUMES).produces(DEFAULT_PRODUCES_AND_CONSUMES).select().build();
	}

	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title(title).description(description).termsOfServiceUrl(urlForTerms)
				.contact(new Contact(contactName, contactUrl, contactMail)).license(license).licenseUrl(licenseUrl)
				.build();
	}
}
