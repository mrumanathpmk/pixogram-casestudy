package com.stackroute.search.util;

public class Constants {

	public static final String API_KEY = "apiKey";
	public static final String Q_STRING = "q";
	public static final String FROM_STRING = "from";
	public static final String TO_STRING = "to";
	public static final String SORT_BY = "sortBy";
	public static final String DOMAINS_STRING = "domains";
	public static final String CLAIMS = "claims";
	
	private Constants() {
		
	}
}
