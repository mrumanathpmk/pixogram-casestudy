package com.stackroute.search.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.search.service.SearchNewsService;
import com.stackroute.search.util.Constants;

import io.swagger.annotations.ApiImplicitParam;
/**
 * 
 * @author Umanath Rajan
 *
 */
@RestController
public class SearchNewsController {

	@Autowired
	private SearchNewsService service;

	@Autowired
	private SimpleDateFormat formatter;

	@GetMapping(value = "/api/v1/search", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<?> getSearchResult(@RequestParam(Constants.Q_STRING) String qString,
			@RequestParam(value = Constants.FROM_STRING, required = false) String from,
			@RequestParam(value = Constants.TO_STRING, required = false) String to) {
		ResponseEntity<?> result = null;
		try {
			if ((!StringUtils.hasText(from) && StringUtils.hasText(to))
					|| (StringUtils.hasText(from) && !StringUtils.hasText(to))) {
				result = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			} else if (StringUtils.hasText(from) && StringUtils.hasText(to)) {
				if (isValidDateFormat(from) && isValidDateFormat(to) && isValidRange(from, to)) {
					result = new ResponseEntity<>(service.search(qString, from, to).getArticles(), HttpStatus.OK);
				} else {
					result = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
				}

			} else {
				result = new ResponseEntity<>(service.search(qString).getArticles(), HttpStatus.OK);
			}
		} catch (Exception e) {
			result = new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	private boolean isValidDateFormat(String date) {
		try {
			formatter.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	private boolean isValidRange(String from, String to) {
		try {
			Date fromDate = formatter.parse(from);
			Date toDate = formatter.parse(to);
			return fromDate.before(toDate);
		} catch (ParseException e) {
			return false;
		}

	}
}
