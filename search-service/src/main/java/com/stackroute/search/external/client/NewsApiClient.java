package com.stackroute.search.external.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.stackroute.search.model.NewsObject;

@FeignClient(name = "${client.news-api.name}", url = "${client.news-api.url}")
public interface NewsApiClient {
	
	@RequestMapping(value="${client.news-api.api.everything}", method = RequestMethod.GET)
	NewsObject search(@RequestParam(value = "apiKey") String apiKey, @RequestParam(value = "q") String qString);

	@RequestMapping(value="${client.news-api.api.everything}", method = RequestMethod.GET)
	NewsObject search(@RequestParam(value = "apiKey") String apiKey, @RequestParam(value = "q") String qString,
			@RequestParam("from") String from, @RequestParam("to") String to);
}
