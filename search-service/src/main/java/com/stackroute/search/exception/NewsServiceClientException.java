package com.stackroute.search.exception;

public class NewsServiceClientException extends Exception {

	public NewsServiceClientException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7715610468219095672L;

}
