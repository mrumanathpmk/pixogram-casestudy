package com.stackroute.userservice.controller;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class UserControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private UserService userService;
	
	private User user;
	
	@InjectMocks
	private UserController userController;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(userController).build();
		user = new User();
		user.setFirstname("prabakaran");
		user.setLastname("loganathan");
		user.setUsername("praba");
		user.setPassword("praba123");
		user.setUserRole("admin");
		user.setUserAddedDate(new Date());
	}
	
	@Test
    public void testRegisterUserSuccess() throws Exception {

        Mockito.when(userService.registerUser(user)).thenReturn(user);
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/register").contentType(MediaType.APPLICATION_JSON).content(jsonToString(user)))
                .andExpect(MockMvcResultMatchers.status().isCreated()).andDo(MockMvcResultHandlers.print());
    }
	
	@Test
    public void testRegisterUserFailure() throws Exception {
		UserAlreadyExistException ex = Mockito.mock(UserAlreadyExistException.class);
        Mockito.when(userService.registerUser(Mockito.any(User.class))).thenThrow(ex);
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/register").contentType(MediaType.APPLICATION_JSON).content(jsonToString(user)))
                .andExpect(MockMvcResultMatchers.status().isConflict());
    }
	
	@Test
    public void testUpdate() throws Exception {
        Mockito.when(userService.registerUser(user)).thenReturn(user);
        mvc.perform(MockMvcRequestBuilders.put("/api/v1/user").contentType(MediaType.APPLICATION_JSON).content(jsonToString(user)))
                .andExpect(MockMvcResultMatchers.status().isNotImplemented());
    }
	
	@Test
    public void testLoginSuccess() throws Exception {

        Mockito.when(userService.findByUsernameAndPassword(user.getUsername(), user.getPassword())).thenReturn(user);
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login").contentType(MediaType.APPLICATION_JSON).content(jsonToString(user)))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print());
    }

	@Test
    public void testLoginFailure() throws Exception {

        Mockito.when(userService.findByUsernameAndPassword(user.getUsername(), user.getPassword())).thenThrow(UserNotFoundException.class);
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login").contentType(MediaType.APPLICATION_JSON).content(jsonToString(user)))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized()).andDo(MockMvcResultHandlers.print());
    }
	
	@Test
	public void testisAvaiableUsernameTrue() throws Exception {

        Mockito.when(userService.isAvailableUsername("praba123")).thenReturn(true);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/auth/check/praba123"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string("true"));
    }
	
	@Test
	public void testisAvaiableUsernameFalse() throws Exception {

        Mockito.when(userService.isAvailableUsername("praba123")).thenReturn(false);
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/auth/check/praba123"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().string("false"));
    }
	
	/** Parsing String format data into JSON format
	 * 
	 * @param obj
	 * @return
	 * @throws JsonProcessingException
	 */
    private static String jsonToString(final Object obj) throws JsonProcessingException {
        String result;
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            result = jsonContent;
        } catch (JsonProcessingException e) {
            result = "Json processing error";
        }
        return result;
    }

}
