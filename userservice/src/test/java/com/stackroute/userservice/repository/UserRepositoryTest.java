package com.stackroute.userservice.repository;

import java.util.Date;
import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stackroute.userservice.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	private User user;

	@Before
	public void setUp() {
		user = new User();
		user.setFirstname("prabakaran");
		user.setLastname("loganathan");
		user.setUsername("praba");
		user.setPassword("praba123");
		user.setUserRole("admin");
		user.setUserAddedDate(new Date());
	}

	@After
	public void tearDown() {
		userRepository.deleteAll();
	}

	@Test
	public void registerUserSuccess() {
		userRepository.save(user);
		User userReturned = userRepository.getOne(user.getUsername());
		Assert.assertNotNull(userReturned);
		Assert.assertEquals(user.getUsername(), userReturned.getUsername());
	}

	@Test
	public void deleteSuccess() {
		userRepository.save(user);
		User userReturned = userRepository.getOne(user.getUsername());
		Assert.assertNotNull(userReturned);
		userRepository.deleteById(user.getUsername());
		Assert.assertFalse(userRepository.findById(user.getUsername()).isPresent());
	}

	@Test
	public void loginSuccess() {
		userRepository.save(user);
		Optional<User> valueInDb = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		Assert.assertTrue(valueInDb.isPresent());
	}
	
	@Test
	public void loginFailure() {
		userRepository.save(user);
		Optional<User> valueInDb = userRepository.findByUsernameAndPassword(user.getUsername(), "");
		Assert.assertFalse(valueInDb.isPresent());
	}
}
