package com.stackroute.userservice.service;

import java.util.Date;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.repository.UserRepository;
import com.stackroute.userservice.service.impl.UserServiceImpl;

public class UserServiceImplTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserServiceImpl userService;

	private User user;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		user = new User();
		user.setFirstname("prabakaran");
		user.setLastname("loganathan");
		user.setUsername("praba");
		user.setPassword("praba123");
		user.setUserRole("admin");
		user.setUserAddedDate(new Date());
	}

	@Test
	public void registerUserSuccess() throws UserAlreadyExistException {
		User userReturned = userService.registerUser(user);
		Assert.assertEquals(user, userReturned);
	}

	@Test(expected = UserAlreadyExistException.class)
	public void registerUserFailure() throws UserAlreadyExistException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(true);
		userService.registerUser(user);
		Assert.fail("Expected " + UserAlreadyExistException.class.getName());
	}

	@Test
	public void updateUserSuccess() throws UserNotFoundException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(true);
		User userReturned = userService.updateUser(user);
		Assert.assertEquals(user, userReturned);
	}

	@Test(expected = UserNotFoundException.class)
	public void updateUserFailure() throws UserNotFoundException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(false);
		userService.updateUser(user);
		Assert.fail("Expected " + UserNotFoundException.class.getName());
	}

	@Test
	public void getUserSuccess() throws UserNotFoundException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(true);
		Mockito.when(userRepository.getOne(user.getUsername())).thenReturn(user);
		User userReturned = userService.getUser(user.getUsername());
		Assert.assertEquals(user, userReturned);
	}

	@Test(expected = UserNotFoundException.class)
	public void getUserFailure() throws UserNotFoundException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(false);
		userService.getUser(user.getUsername());
		Assert.fail("Expected " + UserNotFoundException.class.getName());
	}

	@Test
	public void deleteUserSuccess() throws UserNotFoundException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(true);
		Mockito.when(userRepository.getOne(user.getUsername())).thenReturn(user);
		Assert.assertTrue(userService.deleteUser(user.getUsername()));
	}

	@Test(expected = UserNotFoundException.class)
	public void deleteUserFailure() throws UserNotFoundException {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(false);
		userService.deleteUser(user.getUsername());
		Assert.fail("Expected " + UserNotFoundException.class.getName());
	}

	@Test
	public void isAvailableUsernameTrue() {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(true);
		Assert.assertTrue(userService.isAvailableUsername(user.getUsername()));
	}

	@Test
	public void isAvailableUsernameFalse() {
		Mockito.when(userRepository.existsById(user.getUsername())).thenReturn(false);
		Assert.assertFalse(userService.isAvailableUsername(user.getUsername()));
	}

	@Test
	public void findByUsernameAndPasswordSuccess() throws UserNotFoundException {
		Mockito.when(userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword())).thenReturn(Optional.of(user));
		User userInDb = userService.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		Assert.assertEquals(userInDb, user);
	}

	@Test(expected = UserNotFoundException.class)
	public void findByUsernameAndPasswordFailure() throws UserNotFoundException {
		Mockito.when(userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword())).thenReturn(Optional.ofNullable(null));
		userService.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		Assert.fail("Expected " + UserNotFoundException.class.getName());
	}
}
