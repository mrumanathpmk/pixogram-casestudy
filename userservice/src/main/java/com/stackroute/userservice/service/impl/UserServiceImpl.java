package com.stackroute.userservice.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.repository.UserRepository;
import com.stackroute.userservice.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;
	
	@Override
	public User registerUser(User user) throws UserAlreadyExistException {
		if(isAvailableUsername(user.getUsername())) {
			throw new UserAlreadyExistException("username already exists");
		}
		repository.saveAndFlush(user);
		return user;
	}

	@Override
	public User updateUser(User user) throws UserNotFoundException {
		if(!isAvailableUsername(user.getUsername())) {
			throw new UserNotFoundException("username does not exist");
		}
		repository.save(user);
		return user;
	}

	@Override
	public User getUser(String username) throws UserNotFoundException {
		if(!isAvailableUsername(username)) {
			throw new UserNotFoundException("username does not exist");
		}
		return repository.getOne(username);
	}

	@Override
	public boolean deleteUser(String username) throws UserNotFoundException {
		if(!isAvailableUsername(username)) {
			throw new UserNotFoundException("username does not exist");
		}
		repository.deleteById(username);
		return true;
	}

	@Override
	public boolean isAvailableUsername(String username) {
		return repository.existsById(username);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) throws UserNotFoundException {
		Optional<User> userInDb = repository.findByUsernameAndPassword(username, password);
		if(!userInDb.isPresent()) {
			throw new UserNotFoundException("username does not exist");
		}
		return userInDb.get();
	}

}
