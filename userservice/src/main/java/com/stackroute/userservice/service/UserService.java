package com.stackroute.userservice.service;

import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;

public interface UserService {

	User registerUser(User user) throws UserAlreadyExistException;
	
	User updateUser(User user) throws UserNotFoundException;
	
	User getUser(String username) throws UserNotFoundException;
	
	boolean deleteUser(String username) throws UserNotFoundException;
	
	boolean isAvailableUsername(String username);
	
	User findByUsernameAndPassword(String username, String password) throws UserNotFoundException;
}
