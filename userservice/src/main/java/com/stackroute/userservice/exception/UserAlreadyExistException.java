package com.stackroute.userservice.exception;

public class UserAlreadyExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8361880961832542579L;

	public UserAlreadyExistException(String message) {
		super(message);
	}
}
