package com.stackroute.userservice.exception;

public class UserNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4264912723095265604L;

	public UserNotFoundException(String message) {
		super(message);
	}

}
