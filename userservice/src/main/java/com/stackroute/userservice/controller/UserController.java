package com.stackroute.userservice.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.stackroute.userservice.exception.UserAlreadyExistException;
import com.stackroute.userservice.exception.UserNotFoundException;
import com.stackroute.userservice.model.User;
import com.stackroute.userservice.service.UserService;
import com.stackroute.userservice.util.Constants;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.ApiImplicitParam;

/**
 * REST APIs to manage user profile and authentication
 * 
 * @author Umanath Rajan
 *
 */
@RestController
public class UserController {

	/** Token expire time in millseconds */
	@Value("${token.expire.time.millseconds:300000}")
	private long expireTimeInMillseconds;

	/** for userservice */
	@Autowired
	private UserService service;

	@PostMapping("/api/v1/auth/register")
	public ResponseEntity<Object> register(final @RequestBody User user) {
		ResponseEntity<Object> result = new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			user.setUserAddedDate(new Date());
			service.registerUser(user);
			final UriComponents uri = UriComponentsBuilder.newInstance()
					.userInfo(user.getFirstname() + " " + user.getLastname())
					.pathSegment("api", "v1", "user", user.getUsername()).build();
			result = new ResponseEntity<Object>(uri, HttpStatus.CREATED);
		} catch (UserAlreadyExistException e) {
			result = new ResponseEntity<Object>("username already exist. Please choose different one!",
					HttpStatus.CONFLICT);
		}
		return result;
	}

	@PutMapping("/api/v1/user")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<HttpStatus> updateUser(final @RequestBody User user) {
		return new ResponseEntity<HttpStatus>(HttpStatus.NOT_IMPLEMENTED);
	}

	@GetMapping("/api/v1/user")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<User> getUser(final HttpServletRequest req) {
		ResponseEntity<User> result = new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
			if (null == claims || null == claims.getSubject()) {
				throw new UserNotFoundException("Invalid Request");
			}
			final User user = service.getUser(claims.getSubject());
			user.setPassword("**********"); // mask password
			result = new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			result = new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		return result;
	}

	@DeleteMapping("/api/v1/user/{username}")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<Boolean> deleteUser(final HttpServletRequest req, @PathVariable("username") String username) {
		ResponseEntity<Boolean> result = new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
			if (null == claims || null == claims.getSubject()) {
				throw new UserNotFoundException("Invalid Request");
			}
			final User user = service.getUser(claims.getSubject());
			if (null == user || !"admin".equalsIgnoreCase(user.getUserRole())) {
				throw new UserNotFoundException("Invalid Request");
			}
			result = new ResponseEntity<Boolean>(service.deleteUser(username), HttpStatus.OK);
		} catch (UserNotFoundException e) {
			result = new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST);
		}
		return result;
	}

	@PostMapping("/api/v1/auth/login")
	public ResponseEntity<Object> login(final @RequestBody User user) {
		ResponseEntity<Object> result = new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			User userFinal = service.findByUsernameAndPassword(user.getUsername(), user.getPassword());
			Map<String, String> out = new HashMap<String, String>();
			out.put("token", getToken(user.getUsername(), user.getPassword()));
			out.put("username", userFinal.getUsername());
			out.put("firstname", userFinal.getFirstname());
			out.put("lastname", userFinal.getLastname());
			out.put("role", userFinal.getUserRole());
			result = new ResponseEntity<Object>(out, HttpStatus.OK);
		} catch (UserNotFoundException e) {
			result = new ResponseEntity<Object>("Invalid Crendetials!", HttpStatus.UNAUTHORIZED);
		} catch (ServletException e) {
			result = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		return result;
	}

	@GetMapping("/api/v1/auth/check/{username}")
	public ResponseEntity<Boolean> isAvaiableUsername(@PathVariable("username") String username) {
		return new ResponseEntity<Boolean>(service.isAvailableUsername(username), HttpStatus.OK);
	}

	// Generate JWT token
	private String getToken(String username, String password) throws ServletException {
		if (!StringUtils.hasText(username) || !StringUtils.hasText(password)) {
			throw new ServletException("Username and Pasword should be not empty!");
		}
		return Jwts.builder().setSubject(username).setIssuedAt(new Date()).setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + expireTimeInMillseconds))
				.signWith(SignatureAlgorithm.HS512, "secretKey").compact();
	}
}
