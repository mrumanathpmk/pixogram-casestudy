import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { RegisterComponent } from './register.component';
import { AlertComponent } from '../alert/alert.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthenticationService } from '../service/authentication.service';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { fakeSchedulers } from 'rxjs-marbles/jasmine/angular';
import { By } from '@angular/platform-browser';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let authenticationService: AuthenticationService;
  let userService: UserService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RegisterComponent,
        AlertComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    authenticationService = fixture.debugElement.injector.get(AuthenticationService);
    router = fixture.debugElement.injector.get(Router);
    userService = fixture.debugElement.injector.get(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should register', fakeSchedulers(() => {
    component.f.firstname.setValue('prabakaran');
    component.f.lastname.setValue('loganathan');
    component.f.username.setValue('praba');
    component.f.password.setValue('admin123');

    spyOn(authenticationService, 'isAvaiableUsername').and.returnValue(of(false));
    spyOn(userService, 'register').and.returnValue(of({
      firstname: 'prabakaran',
      lastname: 'loganathan',
      username: 'praba',
      token: '123456'
    }));
    spyOn(router, 'navigate').and.stub();
    fixture.debugElement.nativeElement.querySelector('button').click();
    tick(300);
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
  }));

  it('should register -failed due to invalid inputs', fakeSchedulers(() => {
    component.f.firstname.setValue('');
    component.f.lastname.setValue('');
    component.f.username.setValue('');
    component.f.password.setValue('');

    spyOn(authenticationService, 'isAvaiableUsername').and.returnValue(of(true));

    spyOn(router, 'navigate').and.stub();
    const e = fixture.debugElement.query(By.css('.usernameCss')).nativeElement;
    e.dispatchEvent(new Event('blur'));
    fixture.debugElement.nativeElement.querySelector('button').click();
    tick(300);
    fixture.detectChanges();
    expect(router.navigate).not.toHaveBeenCalledWith(['/login']);
  }));
});
