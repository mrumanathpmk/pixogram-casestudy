import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../service/authentication.service';
import { UserService } from '../service/user.service';
import { AlertService } from '../service/alert.service';

@Component({
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', { validators: [Validators.required, Validators.minLength(3)], updateOn: 'blur' }],
      lastname: ['', { validators: [Validators.required, Validators.minLength(3)], updateOn: 'blur' }],
      username: ['', { validators: [Validators.required, this.validateUsername()], updateOn: 'blur' }],
      password: ['', { validators: [Validators.required, Validators.minLength(6)], updateOn: 'blur' }]
    });

  }
  private validateUsername(): ValidatorFn {
    const fn = (control: AbstractControl): ValidationErrors | null => {
      if (control.value) {
        this.authenticationService.isAvaiableUsername(control.value)
          .subscribe(
            data => {
              if (data === true) {
                console.log('true==');
                control.setErrors({ 'alreadyExist': true });
                return { 'alreadyExist': true };
              } else {
                return null;
              }
            }
          );
      }
      return null;
    };
    return fn;
  }
  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.register(this.registerForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Registration successful', true);
          this.router.navigate(['/login']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
