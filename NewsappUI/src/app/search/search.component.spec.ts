import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { SearchComponent } from './search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatCardModule,
  MatIconModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatInputModule,
  MatSnackBar
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AlertComponent } from '../alert/alert.component';
import { SearchService } from '../service/search.service';
import { Article } from '../model/article';
import { By } from '@angular/platform-browser';
import { of, throwError } from 'rxjs';
import { fakeSchedulers } from 'rxjs-marbles/jasmine/angular';
import { FavouriteService } from '../service/favourite.service';
import { HttpErrorResponse } from '@angular/common/http';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let searchService: SearchService;
  let snackBar: MatSnackBar;
  let favourites: Array<Article>;
  let favouriteService: FavouriteService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SearchComponent,
        AlertComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatInputModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    favourites = [{
      id: '1',
      author: 'prabakaran',
      title: 'unit testing',
      description: 'unit testing description',
      content: 'my content',
      url: 'http://news-url.com',
      publishedAt: '10/10/2019',
      urlToImage: 'http://news-url.com/images/1'
    },
    {
      id: '2',
      author: 'mithun',
      title: 'angular testing',
      description: 'angular testing description',
      content: 'my test case content',
      url: 'http://news-us-url.com',
      publishedAt: '11/10/2019',
      urlToImage: 'http://news-us-url.com/images/2'
    }];
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    searchService = fixture.debugElement.injector.get(SearchService);
    snackBar = fixture.debugElement.injector.get(MatSnackBar);
    favouriteService = fixture.debugElement.injector.get(FavouriteService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populate result', fakeSchedulers(() => {
    spyOn(searchService, 'search').and.returnValue(of(favourites));
    const element = fixture.debugElement.query(By.css('.search-box')).nativeElement;
    element.value = 'apple';
    element.dispatchEvent(new Event('input', { bubbles: true }));
    fixture.detectChanges();
    tick(400);
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(2).toEqual(elements.length);
    expect(elements[0].query(By.css('.mat-card-subtitle'))
      .nativeElement.textContent)
      .toContain(favourites[0].author, 'should contain');
    expect(elements[1].query(By.css('.mat-card-title'))
      .nativeElement.textContent)
      .toContain(favourites[1].title, 'should contain');
  }));

  it('should add to favourites', fakeSchedulers(() => {
    spyOn(searchService, 'search').and.returnValue(of(favourites));
    const element = fixture.debugElement.query(By.css('.search-box')).nativeElement;
    element.value = 'apple';
    element.dispatchEvent(new Event('input', { bubbles: true }));
    fixture.detectChanges();
    tick(400);
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(2).toEqual(elements.length);
    expect(
      elements[0].query(By.css('.mat-card-subtitle')).nativeElement.textContent).toContain(
        favourites[0].author, 'should contain');
    expect(
      elements[1].query(By.css('.mat-card-title')).nativeElement.textContent)
      .toContain(favourites[1].title, 'should contain');
    const favBtn = elements[0].nativeElement.querySelector('button');
    spyOn(favouriteService, 'addFavourite').and.returnValue(of(favourites[0]));
    favBtn.click();
    tick(200);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect('Article is successfully added into your favourite list.')
        .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message);
    });

  }));

  it('should add to favourites - failed by bad request', fakeSchedulers(() => {
    spyOn(searchService, 'search').and.returnValue(of(favourites));
    const element = fixture.debugElement.query(By.css('.search-box')).nativeElement;
    element.value = 'apple';
    element.dispatchEvent(new Event('input', { bubbles: true }));
    fixture.detectChanges();
    tick(400);
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(2).toEqual(elements.length);
    expect(
      elements[0].query(By.css('.mat-card-subtitle')).nativeElement.textContent)
      .toContain(favourites[0].author, 'should contain');
    expect(
      elements[1].query(By.css('.mat-card-title')).nativeElement.textContent)
      .toContain(favourites[1].title, 'should contain');
    const favBtn = elements[0].nativeElement.querySelector('button');
    spyOn(favouriteService, 'addFavourite').and.returnValue(throwError(new HttpErrorResponse({
      error: 'BAD REQUEST',
      status: 400,
      statusText: 'Article already exists in your favourite list',
      url: 'http://localhost:9000/api/v1/favourite'
    })));
    favBtn.click();
    tick(200);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect('Http failure response for http://localhost:9000/api/v1/favourite: 400 Article already exists in your favourite list')
        .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message);
    });
  }));

});
