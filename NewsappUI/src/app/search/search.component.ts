import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SearchService } from '../service/search.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FavouriteService } from '../service/favourite.service';
import { MatSnackBar } from '@angular/material';
import { AlertService } from '../service/alert.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  results: any[] = [];
  queryField: FormControl = new FormControl();
  constructor(
    private searchService: SearchService,
    private favouriteService: FavouriteService,
    private snackBar: MatSnackBar,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.queryField.valueChanges.pipe(debounceTime(200), distinctUntilChanged())
      .subscribe(queryString => this.searchService.search(queryString).subscribe((result: []) => {
        this.results = result;
      })
      );
  }

  addFavourite(index: number) {
    const favourite = this.results[index];
    this.favouriteService.addFavourite(favourite).subscribe(data => {
      this.snackBar.open('Article is successfully added into your favourite list.', '', { duration: 3000 });
    }, error => {
      this.snackBar.open((error.message) ? error.message : error, '', { duration: 3000 });
    });
  }

}
