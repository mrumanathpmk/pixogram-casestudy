import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AlertService } from '../service/alert.service';
import { AlertComponent } from './alert.component';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

describe('AlertComponent', () => {
    let component: AlertComponent;
    let fixture: ComponentFixture<AlertComponent>;
    let alertService: AlertService;
    let element: any;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AlertComponent],
            imports: [
                RouterTestingModule
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AlertComponent);
        component = fixture.componentInstance;
        alertService = fixture.debugElement.injector.get(AlertService);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create alert sucess css', () => {
        spyOn(alertService, 'getMessage').and.returnValue(of({
            type: 'success',
            text: 'success message for you'
        }));
        component.ngOnInit();
        fixture.detectChanges();
        element = fixture.debugElement.query(By.css('.alert-success'));
        expect('success message for you').toEqual(element.nativeElement.textContent, 'should be equal');
    });

    it('should create alert-danger css', () => {
        spyOn(alertService, 'getMessage').and.returnValue(of({
            type: 'error',
            text: 'error message for you'
        }));
        component.ngOnInit();
        fixture.detectChanges();
        element = fixture.debugElement.query(By.css('.alert-danger'));
        expect('error message for you').toEqual(element.nativeElement.textContent, 'should be equal');
    });
});
