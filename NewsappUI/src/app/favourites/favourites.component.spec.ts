import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouritesComponent } from './favourites.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule, MatButtonModule, MatCardModule, MatSnackBarModule, MatSnackBar } from '@angular/material';
import { AlertComponent } from '../alert/alert.component';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { FavouriteService } from '../service/favourite.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Article } from '../model/article';
import { By } from '@angular/platform-browser';
import { of, throwError } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('FavouritesComponent', () => {
  let component: FavouritesComponent;
  let fixture: ComponentFixture<FavouritesComponent>;
  let favouriteService: FavouriteService;
  let favourites: Array<Article>;
  let element: any;
  let snackBar: MatSnackBar;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FavouritesComponent,
        AlertComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        HttpClientModule,
        MatSnackBarModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    favourites = [{
      id: '1',
      author: 'prabakaran',
      title: 'unit testing',
      description: 'unit testing description',
      content: 'my content',
      url: 'http://news-url.com',
      publishedAt: '10/10/2019',
      urlToImage: 'http://news-url.com/images/1'
    },
    {
      id: '2',
      author: 'mithun',
      title: 'angular testing',
      description: 'angular testing description',
      content: 'my test case content',
      url: 'http://news-us-url.com',
      publishedAt: '11/10/2019',
      urlToImage: 'http://news-us-url.com/images/2'
    }];

    fixture = TestBed.createComponent(FavouritesComponent);
    component = fixture.componentInstance;
    favouriteService = fixture.debugElement.injector.get(FavouriteService);
    snackBar = fixture.debugElement.injector.get(MatSnackBar);
    spyOn(favouriteService, 'getAllFavourites').and.returnValue(of(favourites));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populate favourites', () => {
    element = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(2).toEqual(element.length, 'should be equal');
    const title = fixture.debugElement.query(By.css('.mat-card-content'));
    expect(favourites[0].content).toEqual(title.nativeElement.textContent);
  });

  it('should remove article', () => {
    spyOn(favouriteService, 'removeFavourite').and.returnValue(of(true));
    component.removeFavourite('1');
    favourites = favourites.filter(article => article.id !== '1');
    component.favourites = favourites;
    fixture.detectChanges();
    element = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(1).toEqual(element.length, 'should be equal');
    expect(snackBar._openedSnackBarRef).toBeTruthy();
    expect('Article is successfully removed from your favourite list')
      .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message, 'should be equal');
  });

  it('should remove article failed', () => {
    spyOn(favouriteService, 'removeFavourite').and.returnValue(of(false));
    component.removeFavourite('1');
    favourites = favourites.filter(article => article.id !== '1');
    component.favourites = favourites;
    fixture.detectChanges();
    element = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(1).toEqual(element.length, 'should be equal');
    expect(snackBar._openedSnackBarRef).toBeTruthy();
    expect('Article is failed to remove from your favourite list')
      .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message, 'should be equal');
  });

  it('should remove article - server error', () => {
    spyOn(favouriteService, 'removeFavourite').and.returnValue(throwError(new HttpErrorResponse({
      status: 0,
      error: 'Internal Server Error',
      statusText: 'Internal Server Error',
      url: 'http://localhost:9000/api/v1/favourite'
    })));
    component.removeFavourite('1');
    favourites = favourites.filter(article => article.id !== '1');
    component.favourites = favourites;
    fixture.detectChanges();
    element = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(1).toEqual(element.length, 'should be equal');
    expect(snackBar._openedSnackBarRef).toBeTruthy();
    expect('Http failure response for http://localhost:9000/api/v1/favourite: 0 Internal Server Error')
      .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message, 'should be equal');
  });

});
