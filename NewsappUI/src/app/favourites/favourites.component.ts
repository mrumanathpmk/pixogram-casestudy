import { Component, OnInit } from '@angular/core';
import { Article } from '../model/article';
import { FavouriteService } from '../service/favourite.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  favourites: Article[];

  constructor(
    private favouriteService: FavouriteService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.favouriteService.getAllFavourites().subscribe(favourites => this.favourites = favourites);
  }

  removeFavourite(articleId: string) {
    this.favouriteService.removeFavourite(articleId).subscribe(data => {
      if (data) {
        this.snackBar.open('Article is successfully removed from your favourite list', '', { duration: 3000 });
      } else {
        this.snackBar.open('Article is failed to remove from your favourite list', '', { duration: 3000 });
      }
    }, error => {
      this.snackBar.open((error.message) ? error.message : error, '', { duration: 3000 });
    }

    );
  }

}
