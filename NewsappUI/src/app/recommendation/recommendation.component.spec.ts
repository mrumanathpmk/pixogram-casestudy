import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RecommendationComponent } from './recommendation.component';
import { AlertComponent } from '../alert/alert.component';
import {
  MatFormFieldModule,
  MatCardModule,
  MatIconModule,
  MatButtonModule,
  MatSnackBar,
  MatSnackBarModule
} from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Article } from '../model/article';
import { RecommendationService } from '../service/recommendation.service';
import { of, throwError } from 'rxjs';
import { By } from '@angular/platform-browser';
import { FavouriteService } from '../service/favourite.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpErrorResponse } from '@angular/common/http';

describe('RecommendationComponent', () => {
  let component: RecommendationComponent;
  let fixture: ComponentFixture<RecommendationComponent>;
  let favourites: Array<Article>;
  let recommendationService: RecommendationService;
  let snackBar: MatSnackBar;
  let favouriteService: FavouriteService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RecommendationComponent,
        AlertComponent
      ],
      imports: [
        MatFormFieldModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        RouterTestingModule,
        BrowserAnimationsModule
      ]
    })
      .compileComponents();
  }));
  const errorResponse = new HttpErrorResponse({
    error: 'failed',
    statusText: 'article already exists for your favourite list',
    status: 400
  });

  beforeEach(() => {
    favourites = [{
      id: '1',
      author: 'prabakaran',
      title: 'unit testing',
      description: 'unit testing description',
      content: 'my content',
      url: 'http://news-url.com',
      publishedAt: '10/10/2019',
      urlToImage: 'http://news-url.com/images/1'
    },
    {
      id: '2',
      author: 'mithun',
      title: 'angular testing',
      description: 'angular testing description',
      content: 'my test case content',
      url: 'http://news-us-url.com',
      publishedAt: '11/10/2019',
      urlToImage: 'http://news-us-url.com/images/2'
    }];

    fixture = TestBed.createComponent(RecommendationComponent);
    component = fixture.componentInstance;
    recommendationService = fixture.debugElement.injector.get(RecommendationService);
    snackBar = fixture.debugElement.injector.get(MatSnackBar);
    favouriteService = fixture.debugElement.injector.get(FavouriteService);
    spyOn(recommendationService, 'getRecommendedArticles').and.returnValue(of(favourites));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should populate favourites', () => {
    const element = fixture.debugElement.queryAll(By.css('.news-card'));
    expect(2).toEqual(element.length, 'should be equal');
    const title = fixture.debugElement.query(By.css('.mat-card-content'));
    expect(favourites[0].content).toEqual(title.nativeElement.textContent);
  });

  it('should add to favourites', () => {
    spyOn(favouriteService, 'addFavourite').and.returnValue(of(favourites[0]));
    component.addFavourite(1);
    fixture.detectChanges();
    expect(favouriteService.addFavourite).toHaveBeenCalledTimes(1);
    expect('Article is successfully added into your favourite list.')
      .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message, 'should be equal');
  });

  it('should get failure to add to favourites', () => {
    spyOn(favouriteService, 'addFavourite').and.returnValue(throwError(errorResponse));
    component.addFavourite(1);
    fixture.detectChanges();
    expect(favouriteService.addFavourite).toHaveBeenCalledTimes(1);
    expect(errorResponse.message)
      .toEqual(snackBar._openedSnackBarRef.containerInstance.snackBarConfig.data.message, 'should be equal');
  });

});
