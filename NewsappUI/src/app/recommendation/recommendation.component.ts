import { Component, OnInit } from '@angular/core';
import { FavouriteService } from '../service/favourite.service';
import { Article } from '../model/article';
import { RecommendationService } from '../service/recommendation.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.css']
})
export class RecommendationComponent implements OnInit {
  results: Array<Article> = [];
  constructor(
    private recommendationService: RecommendationService,
    private favouriteService: FavouriteService,
    private snackBar: MatSnackBar) {

  }
  ngOnInit(): void {
    this.recommendationService.getRecommendedArticles().subscribe(data => {
      this.results = data;
    });
  }
  addFavourite(index: number) {
    const favourite = this.results[index];
    this.favouriteService.addFavourite(favourite).subscribe(data => {
      this.snackBar.open('Article is successfully added into your favourite list.', '', { duration: 3000 });
    }, error => {
      this.snackBar.open((error.message) ? error.message : error, '', { duration: 3000 });
    });
  }

}
