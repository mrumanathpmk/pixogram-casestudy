import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Subscription, interval, BehaviorSubject } from 'rxjs';
import { Article } from '../model/article';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RecommendationService implements OnInit {

  private recommendedArticles: Array<Article> = [];
  private recommendedSubject: BehaviorSubject<Article[]>;
  private endpoint = '/api/v1/recommendation';
  constructor(private http: HttpClient) {
    this.recommendedSubject = new BehaviorSubject<Article[]>([]);
    this.fetchRecommendedArticles();
  }

  ngOnInit(): void {
    this.fetchRecommendedArticles();
  }
  fetchRecommendedArticles() {
    this.http.get<Array<Article>>(`${environment.apiUrl}${this.endpoint}`).subscribe(data => {
      this.recommendedArticles = data;
      this.recommendedSubject.next(this.recommendedArticles);
    });
  }

  getRecommendedArticles() {
    return this.recommendedSubject;
  }
}
