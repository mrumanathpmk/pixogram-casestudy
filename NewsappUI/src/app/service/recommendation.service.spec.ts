import { TestBed } from '@angular/core/testing';

import { RecommendationService } from './recommendation.service';
import { of } from 'rxjs';
import { Article } from '../model/article';

let httpClientSpy: { get: jasmine.Spy };
let service: RecommendationService;
let article: Article;

describe('RecommendationService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    article = new Article();
    article.id = '1';
    article.author = 'test';
    article.content = 'my test content';
    article.description = ' junit test case description';
    article.title = 'my title';
    article.url = 'test url';
    article.urlToImage = 'http://localhost:8080';
    httpClientSpy.get.and.returnValue(of([article]));
    service = new RecommendationService(<any>httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return base article', () => {
    service.getRecommendedArticles().subscribe(data => {
      expect(1).toEqual(data.length);
    });
  });
});
