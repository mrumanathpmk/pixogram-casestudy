import { Injectable, OnDestroy } from '@angular/core';
import { Article } from '../model/article';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { RecommendationService } from './recommendation.service';

@Injectable({
  providedIn: 'root'
})
export class FavouriteService implements OnDestroy {
  private favourites: Array<Article>;
  private favouriteSubject: BehaviorSubject<Array<Article>>;
  private endpoint = '/api/v1/favourite';

  constructor(private http: HttpClient,
    private recommendationService: RecommendationService) {
    this.favourites = [];
    this.favouriteSubject = new BehaviorSubject<Article[]>([]);
    this.fetchAllFavourites();
  }
  private fetchAllFavourites() {
    this.http.get<Article[]>(`${environment.apiUrl}${this.endpoint}`).subscribe(data => {
      this.favourites = data;
      this.favouriteSubject.next(this.favourites);
    });
  }

  addFavourite(article: Article) {
    return this.http.post<Article>(`${environment.apiUrl}${this.endpoint}`, article).pipe(tap(newArticle => {
      this.favourites.push(newArticle);
      this.favouriteSubject.next(this.favourites);
      this.recommendationService.fetchRecommendedArticles();
    }));
  }

  removeFavourite(articleId: string) {
    return this.http.delete(`${environment.apiUrl}${this.endpoint}/${articleId}`).pipe(tap(data => {
      if (data) {
        this.favourites = this.favourites.filter(favourite => favourite.id !== articleId);
        this.favouriteSubject.next(this.favourites);
        this.recommendationService.fetchRecommendedArticles();
      }
    }));
  }

  getFavourite(articleId: string) {
    return this.http.get<Article>(`${environment.apiUrl}${this.endpoint}/${articleId}`);
  }

  getAllFavourites() {
    return this.favouriteSubject;
  }

  ngOnDestroy() {
    this.favouriteSubject.unsubscribe();
  }
}
