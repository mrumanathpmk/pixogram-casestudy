import { FavouriteService } from './favourite.service';
import { Article } from '../model/article';
import { of, Observable, pipe } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { RecommendationService } from './recommendation.service';

describe('FavouriteService', () => {
  let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy, delete: jasmine.Spy };
  let service: FavouriteService;
  let favourites: Array<Article>;
  let article: Article;
  let recommendationService: RecommendationService;
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);
    recommendationService = jasmine.createSpyObj('RecommendationService', ['fetchRecommendedArticles']);
    article = new Article();
    article.id = '1';
    article.author = 'test';
    article.content = 'my test content';
    article.description = ' junit test case description';
    article.title = 'my title';
    article.url = 'test url';
    article.urlToImage = 'http://localhost:8080';
    favourites = [article];
    httpClientSpy.get.and.returnValue(of(favourites));
    service = new FavouriteService(<any>httpClientSpy, recommendationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get an article', () => {
    httpClientSpy.get.and.returnValue(of(article));
    service.getFavourite('1').subscribe(data => {
      expect('my test content').toEqual(data.content);
    });
  });

  it('should get Bad request', () => {
    const error = new HttpErrorResponse({
      status: 400,
      error: 'Bad request mocked',
      statusText: 'BAD REQUEST'
    });
    httpClientSpy.get.and.returnValue(of(error));
    service.getFavourite('1').subscribe(data => fail, err => expect('Bad request mocked').toEqual(err.error));
  });

});
