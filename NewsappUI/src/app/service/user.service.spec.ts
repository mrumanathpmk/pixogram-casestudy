import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from '../model/user';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

let httpClientSpy: { post: jasmine.Spy };
let service: UserService;
let user: User;
const errorResponse = new HttpErrorResponse({
  error: 'Bad request',
  status: 400,
  statusText: 'BAD REQUEST'
});
describe('UserService', () => {
  beforeEach(() => {
    user = new User();
    user.username = 'prabakaran';
    user.firstname = 'praba';
    user.lastname = 'karan';
    user.token = '1234567';
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new UserService(<any>httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return user by getAll', () => {
    httpClientSpy.post.and.returnValue(of([user]));
    service.register(user).subscribe(data => {
      expect('1234567').toEqual(data[0].token, 'should be equal');
    }, err => fail);
  });

  it('should return bad request by getAll', () => {
    httpClientSpy.post.and.returnValue(of(errorResponse));
    service.register(user).subscribe(data => fail, error => expect(400).toEqual(error.status, 'should be equal'));
  });

});
