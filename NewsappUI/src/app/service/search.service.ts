import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  private endpoint = '/api/v1/search';
  constructor(private http: HttpClient) { }
  search(queryString: string) {
    return this.http.get(`${environment.apiUrl}${this.endpoint}?q=${queryString}`);
  }
}
