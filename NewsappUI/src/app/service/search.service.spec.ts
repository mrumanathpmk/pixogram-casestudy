import { TestBed } from '@angular/core/testing';

import { SearchService } from './search.service';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

let httpClientSpy: { get: jasmine.Spy };
let service: SearchService;
const errorResponse = new HttpErrorResponse({
  error: 'Bad request',
  status: 400,
  statusText: 'BAD REQUEST'
});
describe('SearchService', () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new SearchService(<any>httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get successful result', () => {
    httpClientSpy.get.and.returnValue(of({ title: 'test', content: 'jasmine test' }));
    service.search('nebula').subscribe(data => {
      expect(data).toBeDefined();
      expect('jasmine test').toEqual(data['content']);
    }, err => fail);
  });

  it('should get bad request', () => {
    httpClientSpy.get.and.returnValue(of(errorResponse));
    service.search('nebula').subscribe(data => fail, err => expect(400).toEqual(err.status));
  });
});
