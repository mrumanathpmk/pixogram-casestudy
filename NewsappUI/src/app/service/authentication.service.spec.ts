import { AuthenticationService } from './authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';

describe('AuthenticationService', () => {
    let httpClientSpy: { post: jasmine.Spy, get: jasmine.Spy };
    let service: AuthenticationService;
    const errorResponse = new HttpErrorResponse({
        error: 'Bad request',
        status: 400,
        statusText: 'BAD REQUEST'
    });

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['post', 'get']);
        service = new AuthenticationService(<any>httpClientSpy);
        let store = {};
        const mockLocalStorage = {
            getItem: (key: string): string => {
                return key in store ? store[key] : null;
            },
            setItem: (key: string, value: string) => {
                store[key] = `${value}`;
            },
            removeItem: (key: string) => {
                delete store[key];
            },
            clear: () => {
                store = {};
            }
        };

        spyOn(localStorage, 'getItem')
            .and.callFake(mockLocalStorage.getItem);
        spyOn(localStorage, 'setItem')
            .and.callFake(mockLocalStorage.setItem);
        spyOn(localStorage, 'removeItem')
            .and.callFake(mockLocalStorage.removeItem);
        spyOn(localStorage, 'clear')
            .and.callFake(mockLocalStorage.clear);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get isAvaiableUsername true', () => {
        httpClientSpy.get.and.returnValue(of(true));
        service.isAvaiableUsername('prabakaran').subscribe(data =>
            expect(data).toEqual(true, 'expected true value')
            , error => fail
        );
    });

    it('should get isAvaiableUsername false', () => {
        httpClientSpy.get.and.returnValue(of(false));
        service.isAvaiableUsername('prabakaran').subscribe(data =>
            expect(data).toEqual(false, 'expected false value')
            , error => fail
        );
    });

    it('should get error response for isAvailableUserName', () => {
        httpClientSpy.get.and.returnValue(of(errorResponse));
        service.isAvaiableUsername('prabakaran').subscribe(() => fail,
            error => expect(error.message).toEqual(
                errorResponse.message, 'error message should be')
        );
    });

    it('should logout and remove token from localstorage', () => {
        localStorage.setItem('currentUser',
            JSON.stringify({
                'id': '1',
                'firstname': 'prabakaran',
                'lastname': 'loganathan',
                'username': 'prabakaran',
                'token': '1234567'
            }));
        service = new AuthenticationService(<any>httpClientSpy);
        expect('prabakaran').toEqual(service.currentUserValue.username, 'should be equal');
        service.logout();
        expect(null).toEqual(service.currentUserValue, 'should be equal to null');
    });

    it('should login', () => {
        httpClientSpy.post.and.returnValue(of(JSON.stringify({
            'id': '1',
            'firstname': 'prabakaran',
            'lastname': 'loganathan',
            'username': 'prabakaran',
            'token': '1234567'
        })));
        service.login('prabakaran', 'praba123').subscribe(result =>
            expect('prabakaran').toEqual(JSON.parse(result.toLocaleString() + '')['firstname'], 'should be equal')
        );
    });
});
