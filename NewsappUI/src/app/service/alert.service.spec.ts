import { AlertService } from './alert.service';
import { of, Observable } from 'rxjs';
import { NavigationStart } from '@angular/router';

describe('AlertService', () => {

  let service: AlertService;
  beforeEach(() => {
    const router = {
      events: of(new NavigationStart(0, ''))
    };
    service = new AlertService(<any>router);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get success message', () => {
    service.success('my success message');
    service.getMessage().subscribe(data => {
      expect('success').toEqual(data['type'], 'should be equal');
      expect('my success message').toEqual(data['message'], 'should be equal');
    });
  });

  it('should get error message', () => {
    service.error('my err message');
    service.getMessage().subscribe(data => {
      expect('error').toEqual(data['type'], 'should be equal');
      expect('my err message').toEqual(data['message'], 'should be equal');
    });
  });

});
