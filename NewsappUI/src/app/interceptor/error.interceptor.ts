import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../service/authentication.service';
import { AlertService } from '../service/alert.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private alertService: AlertService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 0 || err.status === 500) {
                this.alertService.error('Backend service is down. Please try after sometime.');
            } else if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.alertService.error(err.error);
                if (!request.url.includes('/api/v1/auth')) {
                    this.authenticationService.logout();
                    // location.reload();
                }
            }
            const errorMessage = err.error || err.statusText;
            return throwError(errorMessage);
        }));
    }
}
