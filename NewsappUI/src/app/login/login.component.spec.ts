import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertComponent } from '../alert/alert.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthenticationService } from '../service/authentication.service';
import { By } from '@angular/platform-browser';
import { of, throwError } from 'rxjs';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthenticationService;
  const user = new User();
  user.firstname = 'prabakaran';
  user.lastname = 'loganathan';
  user.username = 'praba';
  user.token = '123456';
  const errorResponse = new HttpErrorResponse({
    error: 'bad crendentials',
    status: 409,
    statusText: 'Invalid crendetials'
  });
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent,
        AlertComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authService = fixture.debugElement.injector.get(AuthenticationService);
    router = fixture.debugElement.injector.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate for valid inputs', () => {
    component.f.username.setValue('prabakaran');
    component.f.password.setValue('admin123');
    spyOn(authService, 'login').and.returnValue(of(user));
    spyOn(router, 'navigate').and.stub();
    component.onSubmit();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalled();
  });

  it('should validate for invalid crendentials', fakeAsync(() => {
    component.f.username.setValue('prabakaran');
    component.f.password.setValue('admin123');
    spyOn(authService, 'login').and.returnValue(throwError(errorResponse));
    spyOn(router, 'navigate').and.stub();
    component.onSubmit();
    tick();
    fixture.detectChanges();
    expect(router.navigate).toHaveBeenCalledTimes(0);
    expect(false).toEqual(component.loading, 'should be equal');
  }));

  it('should validate for invalid inputs', () => {
    component.f.username.setValue('');
    component.f.password.setValue('');
    spyOn(authService, 'login').and.returnValue(of(user));
    spyOn(router, 'navigate').and.stub();
    component.onSubmit();
    fixture.detectChanges();
    const element = fixture.debugElement.queryAll(By.css('.invalid-feedback'));
    expect(2).toEqual(element.length, 'should be equal');
    expect('Username is required').toEqual(element[0].nativeElement.textContent);
    expect('Password is required').toEqual(element[1].nativeElement.textContent);
  });
});
