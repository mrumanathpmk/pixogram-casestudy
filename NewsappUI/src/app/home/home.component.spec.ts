import { async, ComponentFixture, TestBed, fakeAsync, inject } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthenticationService } from '../service/authentication.service';
import { of } from 'rxjs';
import { User } from '../model/user';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { SearchComponent } from '../search/search.component';
import { FavouritesComponent } from '../favourites/favourites.component';
import { RecommendationComponent } from '../recommendation/recommendation.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { AlertComponent } from '../alert/alert.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule, MatCardModule, MatSnackBarModule } from '@angular/material';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let authenticationService: AuthenticationService;
  let user: User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        SearchComponent,
        FavouritesComponent,
        RecommendationComponent,
        LoginComponent,
        RegisterComponent,
        AlertComponent
      ],
      imports: [
        AppRoutingModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatCardModule,
        MatSnackBarModule
      ],
      providers: [

      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    user = new User();
    user.firstname = 'prabakaran';
    user.lastname = 'loganathan';
    user.username = 'praba';
    user.token = '123456';
    fixture = TestBed.createComponent(HomeComponent);
    fixture.detectChanges();
    component = fixture.componentInstance;
    component.currentUser = user;
    authenticationService = fixture.debugElement.injector.get(AuthenticationService);
    localStorage.setItem('currentUser', JSON.stringify(user));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display name of login user', () => {
    spyOn(authenticationService, 'currentUser').and.returnValue(of(user));
    fixture.detectChanges();
    const element = fixture.debugElement.query(By.css('.profile'));
    expect('Hi! ' + user.firstname + ' ' + user.lastname).toEqual(element.nativeElement.textContent, 'should be equal');
  });

  it('should navigate to home', async(inject([Router, Location], (router: Router, location: Location) => {
    fixture.debugElement.queryAll(By.css('.nav-link'))[0].nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/search');
    });
  })));

  it('should navigate to search', async(inject([Router, Location], (router: Router, location: Location) => {
    fixture.debugElement.queryAll(By.css('.nav-link'))[1].nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/search');
    });
  })));

  it('should navigate to favourites', async(inject([Router, Location], (router: Router, location: Location) => {
    fixture.debugElement.queryAll(By.css('.nav-link'))[2].nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/favourites');
    });
  })));

  it('should navigate to recommendation', async(inject([Router, Location], (router: Router, location: Location) => {
    fixture.debugElement.queryAll(By.css('.nav-link'))[3].nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/recommendation');
    });
  })));

  it('should log out', async(inject([Router, Location], (router: Router, location: Location) => {
    fixture.debugElement.queryAll(By.css('.nav-link'))[4].nativeElement.click();
    fixture.detectChanges();
     fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/login');
     });
  })));
});
