import { browser, ElementFinder, element, by, ElementArrayFinder, promise } from 'protractor';

export class SearchPage {
    // navigate to search page
    navigateSearchPage() {
        return browser.get('/search');
    }
    // to pause browser
    pauseBrowser(port) {
        browser.pause(port);
    }
    // app component
    getAppComponent(): ElementFinder {
        return element(by.tagName('app-root'));
    }
    // search compoent
    getSearchComponent(): ElementFinder {
        return element(by.tagName('app-search'));
    }
    // alert component
    getAlertComponent(): ElementFinder {
        return element(by.tagName('app-alert'));
    }
    // search input box
    getSearchInputBox(): ElementFinder {
        return element(by.tagName('input'));
    }
    // is search box available
    isSearchBoxPresent(): promise.Promise<boolean> {
        return this.getSearchComponent().isPresent();
    }
    // get all articles
    getAllArticles(): ElementArrayFinder {
        return element.all(by.css('mat-card'));
    }
    // get last article
    getFirstArticle(): ElementFinder {
        return this.getAllArticles().first();
    }
    // get Add Favourite Button of first element
    getFirstArticleAddFavouriteButton(): ElementFinder {
        return this.getFirstArticle().element(by.tagName('button'));
    }
    // is add favourite button available
    isFirstArticleAddFavouriteButtonAvailable(): promise.Promise<boolean> {
        return this.getFirstArticleAddFavouriteButton().isPresent();
    }
    // get external link of first element
    getFirstArticleLink(): ElementFinder {
        return this.getFirstArticle().element(by.tagName('button'));
    }
    // is external link available
    isFirstArticleLinkAvailable(): promise.Promise<boolean> {
        return this.getFirstArticleAddFavouriteButton().isPresent();
    }
    // get mat snack bar
    getSnackBar(): ElementFinder {
        return element(by.tagName('simple-snack-bar'));
    }
    // check snackbar
    isSnackBarPresent(): promise.Promise<boolean> {
        return this.getSnackBar().isPresent();
    }


}
