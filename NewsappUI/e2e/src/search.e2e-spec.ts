import { SearchPage } from './page-objects/search.po';
import { browser, by, protractor, element } from 'protractor';


describe('search page', () => {
    let page: SearchPage;

    beforeEach(() => {
        page = new SearchPage();
    });

    it('should render search box', () => {
        page.navigateSearchPage();
        expect(page.isSearchBoxPresent).toBeTruthy('search should exist');
    });

    it('should get all articles', () => {
        page.navigateSearchPage();
        page.getSearchInputBox().sendKeys('apple');
        browser.sleep(300);
        expect(page.getAllArticles().count()).toEqual(20, 'should be equal');
    });

    it('should add articles to favourite', () => {
        page.navigateSearchPage();
        page.getSearchInputBox().sendKeys('apple');
        browser.sleep(300);
        expect(page.getAllArticles().count()).toEqual(20, 'should be equal');
        expect(page.isFirstArticleAddFavouriteButtonAvailable()).toBeTruthy(
            ` <button mat-icon-button (click)="addFavourite(i);"> should be available`);
        expect(page.isFirstArticleLinkAvailable()).toBeTruthy(`<a mat-icon-button/> should be available`);
        page.getFirstArticleAddFavouriteButton().click();
        const EC = protractor.ExpectedConditions;
        const snackBar = element(by.tagName('simple-snack-bar'));
        browser.wait(EC.visibilityOf(snackBar), 4000);
        element(by.tagName('simple-snack-bar')).getText().then(function (val) {
            expect(val).toEqual('Article already exists for this username=' + browser.uniqueUserId);
        });
    });

});
