import { AppPage } from './page-objects/app.po';
import { environment } from '../../src/environments/environment';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display title message', () => {
    page.navigateTo();

    expect(page.getTitleText()).toEqual(environment.appTitle);
  });
});
