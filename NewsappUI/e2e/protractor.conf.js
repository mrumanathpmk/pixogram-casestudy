// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
var uniqueUserId;
exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/app.e2e-spec.ts',
    './src/login.e2e-spec.ts',
    './src/search.e2e-spec.ts'
  ],
  capabilities: {
    'browserName': 'chrome',
    chromeOptions: {
      args: [
        // '--no-sandbox',
        // '--headless',
        // '--window-size=1024,768'
      ]
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () { }
  },
  beforeLaunch: function () {
    //uniqueUserId = Math.random();
    uniqueUserId = 'prabakaran';
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    browser.uniqueUserId = uniqueUserId;
  }
};