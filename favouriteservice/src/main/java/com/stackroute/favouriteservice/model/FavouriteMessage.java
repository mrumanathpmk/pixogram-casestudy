package com.stackroute.favouriteservice.model;

public class FavouriteMessage {
	public static enum Action {
		ADD, REMOVE
	}

	private Action action;
	private String username;
	private Article article;

	public FavouriteMessage() {
		super();
	}

	public FavouriteMessage(Action action, String username, Article article) {
		super();
		this.action = action;
		this.username = username;
		this.article = article;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	@Override
	public String toString() {
		return "FavouriteMessage [action=" + action + ", username=" + username + ", article=" + article + "]";
	}

}
