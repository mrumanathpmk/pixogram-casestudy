package com.stackroute.favouriteservice.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class UserArticle {

	@Id
	private String username;
	private List<Article> articles;
	
	public UserArticle() {
		super();
	}
	
	public UserArticle(String username, List<Article> articles) {
		super();
		this.username = username;
		this.articles = articles;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(final String username) {
		this.username = username;
	}
	public List<Article> getArticles() {
		if (null == articles) {
			articles = new ArrayList<Article>();
		}
		return articles;
	}
	public void setArticles(final List<Article> articles) {
		this.articles = articles;
	}
	
	
}
