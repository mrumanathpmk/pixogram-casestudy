package com.stackroute.favouriteservice.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.Logger;

/* Annotate this class with @Aspect and @Component */

@Aspect
@Component
public class LoggingAspect {
	private static final Logger logger = (Logger) LoggerFactory.getLogger("com.stackroute");
	private static final String EXE_CTRL = "execution(* com.stackroute.favouriteservice.controller.*.*(..))";
	/*
	 * Write loggers for each of the methods of User controller, any particular
	 * method will have all the four aspectJ annotation
	 * (@Before, @After, @AfterReturning, @AfterThrowing).
	 */

	@Before(EXE_CTRL)
	public void adviceBefore(final JoinPoint jp) {
		logger.info("Before executing " + jp.toShortString());
		StringBuilder sb = new StringBuilder();
		for (Object obj : jp.getArgs()) {
			sb.append(obj).append(" ");
		}
		logger.info("Args:" + sb.toString().trim());
	}

	@After(EXE_CTRL)
	public void adviceAfter(final JoinPoint jp) {
		logger.info("After executing " + jp.toShortString());
	}

	@AfterReturning(EXE_CTRL)
	public void adviceReturning(final JoinPoint jp) {
		logger.info("request completed " + jp.toLongString());
	}

	@AfterThrowing(value = EXE_CTRL, throwing = "ex")
	public void adviceThrowing(final JoinPoint jp, final Exception ex) {
		logger.info("Exception occured at" + jp.toLongString() + ", " + ex.getMessage());
	}

}
