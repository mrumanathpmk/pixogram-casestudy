package com.stackroute.favouriteservice.exception;

public class ArticleDoesNotExistException extends Exception {

	public ArticleDoesNotExistException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8331962716076941442L;

}
