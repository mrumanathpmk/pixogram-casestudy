package com.stackroute.favouriteservice.exception;

public class ArticleAlreadyExistException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArticleAlreadyExistException(String message) {
		super(message);
	}

}
