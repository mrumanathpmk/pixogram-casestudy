package com.stackroute.favouriteservice.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.favouriteservice.exception.ArticleAlreadyExistException;
import com.stackroute.favouriteservice.exception.ArticleDoesNotExistException;
import com.stackroute.favouriteservice.model.Article;
import com.stackroute.favouriteservice.model.UserArticle;
import com.stackroute.favouriteservice.repository.ArticleRepository;
import com.stackroute.favouriteservice.repository.FavouriteRepository;
import com.stackroute.favouriteservice.service.FavouriteService;

@Service
public class FavouriteServiceImpl implements FavouriteService {

	@Autowired
	private FavouriteRepository repository;

	@Autowired
	private ArticleRepository articleRepository;

	@Override
	public Article addFavourite(String username, Article article) throws ArticleAlreadyExistException {
		Optional<UserArticle> newsInDB = repository.findById(username);
		UserArticle news = null;
		if (!newsInDB.isPresent()) {
			news = new UserArticle(username, new ArrayList<Article>());
		} else {
			news = newsInDB.get();
		}
		Optional<Article> articleInDB = articleRepository.findArticleByAuthorAndTitleAndDescription(
				article.getAuthor(), article.getTitle(), article.getDescription());
		if (!articleInDB.isPresent()) {
			articleRepository.save(article);
		} else {
			article = articleInDB.get();
		}
		for(Article articleTemp: news.getArticles()) {
			if(articleTemp.getId().equals(article.getId())) {
				throw new ArticleAlreadyExistException("Article already exists for this username="+username);
			}
		}
		news.getArticles().add(article);
		repository.save(news);
		return article;
	}

	@Override
	public boolean removeFavourite(String userId, String articleId) throws ArticleDoesNotExistException {
		Optional<UserArticle> newsInDB = repository.findById(userId);
		if (!newsInDB.isPresent()) {
			throw new ArticleDoesNotExistException("Article does not exist for this user");
		} else {
			UserArticle news = newsInDB.get();
			for (Iterator<Article> it = news.getArticles().iterator(); it.hasNext();) {
				Article article = it.next();
				if (article.getId().toString().equalsIgnoreCase(articleId)) {
					it.remove();
					repository.save(news);
					return true;
				}
			}
		}
		throw new ArticleDoesNotExistException("Article does not exist for this user");
	}

	@Override
	public Article getFavourite(String username, String articleId) throws ArticleDoesNotExistException {
		Optional<UserArticle> newsInDB = repository.findById(username);
		if (!newsInDB.isPresent()) {
			throw new ArticleDoesNotExistException("Article does not exist for this user");
		}

		UserArticle news = newsInDB.get();
		for (Iterator<Article> it = news.getArticles().iterator(); it.hasNext();) {
			Article article = it.next();
			if (article.getId().toString().equalsIgnoreCase(articleId)) {
				return article;
			}
		}
		throw new ArticleDoesNotExistException("Article does not exist for this user");
	}

	@Override
	public List<Article> getAllFavourites(String userId) throws ArticleDoesNotExistException {
		Optional<UserArticle> newsInDB = repository.findById(userId);
		if (!newsInDB.isPresent()) {
			throw new ArticleDoesNotExistException("Article does not exist for this user");
		}
		return repository.findById(userId).get().getArticles();
	}

}
