package com.stackroute.favouriteservice.service;

import java.util.List;

import com.stackroute.favouriteservice.exception.ArticleAlreadyExistException;
import com.stackroute.favouriteservice.exception.ArticleDoesNotExistException;
import com.stackroute.favouriteservice.model.Article;

public interface FavouriteService {

	Article addFavourite(String username, Article article) throws ArticleAlreadyExistException;

	boolean removeFavourite(String username, String articleId) throws ArticleDoesNotExistException;

	Article getFavourite(String username, String articleId) throws ArticleDoesNotExistException;

	List<Article> getAllFavourites(String username) throws ArticleDoesNotExistException;

}
