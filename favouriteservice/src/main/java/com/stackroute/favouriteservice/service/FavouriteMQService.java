package com.stackroute.favouriteservice.service;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface FavouriteMQService {
	
	@Output("favouriteMessageChannel")
	MessageChannel favouritePublisher();

}
