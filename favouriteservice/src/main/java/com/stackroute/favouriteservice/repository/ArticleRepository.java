package com.stackroute.favouriteservice.repository;

import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.stackroute.favouriteservice.model.Article;

@Repository
public interface ArticleRepository extends MongoRepository<Article, ObjectId> {

	Optional<Article> findArticleByAuthorAndTitleAndDescription(String author, String title, String description);
}
