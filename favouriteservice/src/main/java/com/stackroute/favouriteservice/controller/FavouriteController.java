package com.stackroute.favouriteservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.favouriteservice.exception.ArticleAlreadyExistException;
import com.stackroute.favouriteservice.exception.ArticleDoesNotExistException;
import com.stackroute.favouriteservice.model.Article;
import com.stackroute.favouriteservice.model.FavouriteMessage;
import com.stackroute.favouriteservice.model.FavouriteMessage.Action;
import com.stackroute.favouriteservice.service.FavouriteMQService;
import com.stackroute.favouriteservice.service.FavouriteService;
import com.stackroute.favouriteservice.util.Constants;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.ApiImplicitParam;

@EnableBinding(FavouriteMQService.class)
@RestController
@RequestMapping("/api/v1/favourite")
public class FavouriteController {

	@Autowired
	private FavouriteService service;

	@Autowired
	private FavouriteMQService mqService;

	// Add
	@PostMapping
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<?> addFavourite(final HttpServletRequest req, final @RequestBody Article article) {
		ResponseEntity<?> result = new ResponseEntity<Article>(HttpStatus.INTERNAL_SERVER_ERROR);
		final Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
		if (null == claims || null == claims.getSubject()) {
			result = new ResponseEntity<String>("BAD request- Invalid headers received", HttpStatus.BAD_REQUEST);
		} else {
			final String username = claims.getSubject();

			try {
				final Article out = service.addFavourite(username, article);
				mqService.favouritePublisher()
						.send(MessageBuilder
								.withPayload(new FavouriteMessage(FavouriteMessage.Action.ADD, username, out))
								.setHeader("username", username).build());
				result = new ResponseEntity<Article>(out, HttpStatus.OK);
			} catch (ArticleAlreadyExistException e) {
				result = new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
			}
		}
		return result;
	}

	// delete
	@DeleteMapping("{id}")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<?> removeFavourite(final HttpServletRequest req, final @PathVariable("id") String articleId) {
		ResponseEntity<?> result = new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		final Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
		if (null == claims || null == claims.getSubject()) {
			result = new ResponseEntity<String>("BAD request- Invalid headers received", HttpStatus.BAD_REQUEST);
		} else {
			final String username = claims.getSubject();
			try {
				final Article article = service.getFavourite(username, articleId);
				mqService.favouritePublisher().send(
						MessageBuilder.withPayload(new FavouriteMessage(Action.REMOVE, username, article)).build());
				final boolean out = service.removeFavourite(username, articleId);
				result = new ResponseEntity<Boolean>(out, HttpStatus.OK);
			} catch (ArticleDoesNotExistException e) {
				result = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
		}
		return result;
	}

	// getall
	@GetMapping
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<?> getAllFavourites(HttpServletRequest req) throws Exception {
		ResponseEntity<?> result = new ResponseEntity<List<Article>>(HttpStatus.INTERNAL_SERVER_ERROR);
		final Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
		if (null == claims || null == claims.getSubject()) {
			result = new ResponseEntity<String>("BAD request- Invalid headers received", HttpStatus.BAD_REQUEST);
		} else {
			final String username = claims.getSubject();
			try {
				result = new ResponseEntity<List<Article>>(service.getAllFavourites(username), HttpStatus.OK);
			} catch (ArticleDoesNotExistException e) {
				result = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
		}
		return result;
	}

	// get
	@GetMapping("{id}")
	@ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer <access_token>")
	public ResponseEntity<?> getFavourite(HttpServletRequest req, @PathVariable("id") String id) throws Exception {
		ResponseEntity<?> result = new ResponseEntity<Article>(HttpStatus.INTERNAL_SERVER_ERROR);
		final Claims claims = (Claims) req.getAttribute(Constants.CLAIMS);
		if (null == claims || null == claims.getSubject()) {
			result = new ResponseEntity<String>("BAD request- Invalid headers received", HttpStatus.BAD_REQUEST);
		} else {
			final String username = claims.getSubject();
			try {
				result = new ResponseEntity<Article>(service.getFavourite(username, id), HttpStatus.OK);
			} catch (ArticleDoesNotExistException e) {
				result = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
		}
		return result;
	}
}
