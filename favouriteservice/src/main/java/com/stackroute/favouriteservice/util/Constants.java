package com.stackroute.favouriteservice.util;

public class Constants {

	public static final String CLAIMS = "claims";
	public static final String RECOMMENDATION_SERVICE_URL = "/api/v1/recommendation";
	public static final String RECOMMENDATION_CLIENT_NAME = "${recommendation.service.name}";

	private Constants() {

	}
}
