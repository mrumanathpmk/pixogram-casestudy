package com.stackroute.favouriteservice.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.stackroute.favouriteservice.exception.ArticleAlreadyExistException;
import com.stackroute.favouriteservice.exception.ArticleDoesNotExistException;
import com.stackroute.favouriteservice.model.Article;
import com.stackroute.favouriteservice.model.UserArticle;
import com.stackroute.favouriteservice.repository.ArticleRepository;
import com.stackroute.favouriteservice.repository.FavouriteRepository;
import com.stackroute.favouriteservice.service.impl.FavouriteServiceImpl;

public class FavouriteServiceImplTest {

	@Mock
	private ArticleRepository articleRepository;

	@Mock
	private FavouriteRepository favouriteRepository;

	private Article article;

	@InjectMocks
	private FavouriteServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		article = new Article();
		article.setId(new ObjectId(new Date()));
		article.setAuthor("praba");
		article.setTitle("junit");
		article.setDescription("junit for test cases");
	}

	@Test
	public void testAddFavouriteNewSuccess() throws ArticleAlreadyExistException {
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.empty());
		when(articleRepository.findArticleByAuthorAndTitleAndDescription(article.getAuthor(), article.getTitle(),
				article.getDescription())).thenReturn(Optional.empty());
		Article result = service.addFavourite("prabakaran", article);
		assertEquals(article, result);

	}

	@Test(expected = ArticleAlreadyExistException.class)
	public void testAddFavouriteFailure() throws ArticleAlreadyExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById(Mockito.anyString())).thenReturn(Optional.of(news));
		when(articleRepository.findArticleByAuthorAndTitleAndDescription(article.getAuthor(), article.getTitle(),
				article.getDescription())).thenReturn(Optional.empty());
		Article result = service.addFavourite("prabakaran", article);
		assertEquals(article, result);
	}
	
	@Test
	public void testRemoveFavouriteSuccess() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.of(news));
		assertTrue(service.removeFavourite("prabakaran", article.getId()));
	}
	
	@Test(expected=ArticleDoesNotExistException.class)
	public void testRemoveFavouriteFailureNoUser() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.empty());
		assertTrue(service.removeFavourite("prabakaran", article.getId()));
	}
	
	@Test(expected=ArticleDoesNotExistException.class)
	public void testRemoveFavouriteFailureNoArticle() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.of(news));
		assertTrue(service.removeFavourite("prabakaran", article.getId()+"1"));
	}
	
	@Test
	public void testGetFavouriteSuccess() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.of(news));
		assertEquals(article, service.getFavourite("prabakaran", article.getId()));
	}
	
	@Test(expected = ArticleDoesNotExistException.class)
	public void testGetFavouriteFailureNoUser() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.empty());
		assertEquals(article, service.getFavourite("prabakaran", article.getId()));
	}
	
	@Test(expected = ArticleDoesNotExistException.class)
	public void testGetFavouriteFailureNoArticle() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.of(news));
		assertEquals(article, service.getFavourite("prabakaran", article.getId()+"2"));
	}
	
	@Test(expected = ArticleDoesNotExistException.class)
	public void testGetAllFavouritesFailureNoUser() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.empty());
		assertEquals(news.getArticles(), service.getAllFavourites("prabakaran1"));
	}
	
	@Test
	public void testGetAllFavouritesSuccess() throws ArticleDoesNotExistException {
		UserArticle news = new UserArticle("prabakaran", null);
		news.getArticles().add(article);
		when(favouriteRepository.findById("prabakaran")).thenReturn(Optional.of(news));
		assertEquals(news.getArticles(), service.getAllFavourites("prabakaran"));
	}
}
