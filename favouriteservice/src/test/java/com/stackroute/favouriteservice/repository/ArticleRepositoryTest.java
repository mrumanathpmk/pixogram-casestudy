package com.stackroute.favouriteservice.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.stackroute.favouriteservice.model.Article;

@RunWith(SpringRunner.class)
@DataMongoTest()
public class ArticleRepositoryTest {

	@Autowired
	private ArticleRepository repository;

	private Article article;

	@Before
	public void setUp() throws Exception {
		article = new Article();
		article.setAuthor("prabakaran");
		article.setTitle("junit");
		article.setDescription("junit test cases");
	}

	@Test
	public void testFindArticleByAuthorAndTitleAndDescription() {
		repository.save(article);
		assertEquals(article.getAuthor(), repository.findArticleByAuthorAndTitleAndDescription(article.getAuthor(),
				article.getTitle(), article.getDescription()).get().getAuthor());
	}

}
