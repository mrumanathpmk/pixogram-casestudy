package com.stackroute.favouriteservice.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.favouriteservice.exception.ArticleAlreadyExistException;
import com.stackroute.favouriteservice.exception.ArticleDoesNotExistException;
import com.stackroute.favouriteservice.model.Article;
import com.stackroute.favouriteservice.service.FavouriteMQService;
import com.stackroute.favouriteservice.service.FavouriteService;
import com.stackroute.favouriteservice.util.Constants;

import io.jsonwebtoken.Claims;

@RunWith(SpringRunner.class)
@WebMvcTest
public class FavouriteControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private FavouriteService service;

	@MockBean
	private FavouriteMQService mqService;

	@InjectMocks
	private FavouriteController controller;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	private Article article;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
		article = new Article();
		article.setId(new ObjectId(new Date()));
		article.setAuthor("prabakaran");
		article.setTitle("junit");
		article.setDescription("junit for test cases");
	}

	@Test
	public void testAddFavouriteBadRequest() throws Exception {
		mvc.perform(
				post("/api/v1/favourite").contentType(MediaType.APPLICATION_JSON_VALUE).content(jsonToString(article)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testAddFavouriteSuccess() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.addFavourite(Mockito.anyString(), Mockito.any(Article.class))).thenReturn(article);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(post("/api/v1/favourite").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(jsonToString(article)))
				.andExpect(status().isOk()).andExpect(content().string(jsonToString(article)));
	}

	@Test
	public void testAddFavouriteConflict() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.addFavourite(Mockito.anyString(), Mockito.any(Article.class)))
				.thenThrow(ArticleAlreadyExistException.class);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(post("/api/v1/favourite").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(jsonToString(article)))
				.andExpect(status().isConflict());
	}

	@Test
	public void testRemoveFavouriteBadRequest() throws Exception {
		mvc.perform(
				delete("/api/v1/favourite/1").contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testRemoveFavouriteSuccess() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.removeFavourite(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(delete("/api/v1/favourite/1").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andExpect(content().string("true"));
	}

	@Test
	public void testRemoveFavouriteFailure() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.removeFavourite(Mockito.anyString(), Mockito.anyString()))
				.thenThrow(ArticleDoesNotExistException.class);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(delete("/api/v1/favourite/1").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testGetAllFavouritesBadRequest() throws Exception {
		mvc.perform(get("/api/v1/favourite").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
	}

	@Test
	public void testGetAllFavouritesSuccess() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		List<Article> articles = new ArrayList<Article>();
		articles.add(article);
		when(service.getAllFavourites(Mockito.anyString())).thenReturn(articles);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(get("/api/v1/favourite").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andExpect(content().string(jsonToString(articles)));
	}
	
	@Test
	public void testGetAllFavouritesFailure() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		List<Article> articles = new ArrayList<Article>();
		articles.add(article);
		when(service.getAllFavourites(Mockito.anyString())).thenThrow(ArticleDoesNotExistException.class);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(get("/api/v1/favourite").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testGetFavouriteBadRequest() throws Exception {
		mvc.perform(get("/api/v1/favourite/1").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isBadRequest());
	}

	@Test
	public void testGetFavouriteSuccess() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.getFavourite(Mockito.anyString(), Mockito.anyString())).thenReturn(article);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(get("/api/v1/favourite/1").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andExpect(content().string(jsonToString(article)));
	}
	
	@Test
	public void testGetFavouriteFailure() throws Exception {
		Claims claims = mock(Claims.class);
		when(claims.getSubject()).thenReturn("prabakaran");
		when(service.getFavourite(Mockito.anyString(), Mockito.anyString())).thenThrow(ArticleDoesNotExistException.class);
		when(mqService.favouritePublisher()).thenReturn(mock(MessageChannel.class));
		mvc.perform(get("/api/v1/favourite/1").requestAttr(Constants.CLAIMS, claims)
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}
	
	/**
	 * Parsing String format data into JSON format
	 * 
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	private static String jsonToString(final Object obj) throws Exception {
		String result;
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(obj);
			result = jsonContent;
		} catch (Exception e) {
			result = "Json processing error";
		}
		return result;
	}
}
